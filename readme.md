WAV to Amiga Resampler Script
=============================

A convenience/QOL script for converting WAV audio samples to all pitches of the Amiga Protracker audio program (8svx).

This helps with reducing the overall size of your project, as some demo competitions such as *cough* boozedrome *cough*
limit artists to a song of 500 kilobytes. Instead of having to tweak things in audacity you can now just run convert.py
and your wav input files will be converted. Only use wav, and certainly not mp3. Mp3 will have padding in the audio.

Kind regards,
Elger/"Stitch"

Installation
------------
Your system needs ffmpeg, sox and python3.8 or higher to be installed. This can be retrieved with brew or apt.
This was written on a mac, so ymmv. `brew install ffmpeg sox python3` and you might want to `xcode-select --install`

What it does
------------
It simply wraps two shitty commands since wE aS a SoCiEtY hAvE fOrGoTtEn HoW tO pRoGrAm.

The commands run are:
- ffmpeg -i example.wav -vn -ar 44100 -ac 1 example.aiff
- sox example.aiff -r22000 -c1 example.8svx

Running
-------
Run `python resample.py` to convert any audio samples from the input directory to the output directory.

Other things
------------
Notes above F3 are basically useless as the amiga can't render that anyway with standard, unexpanded, hardware. So
these outputs are commented out in the source.
(22.000 hz)

Other notices
-------------
- Auslive.wav is public domain by Titus & datucker / rabenauge presented at Outline Demoparty
- List of notes is taken from  https://16-bits.org/ptfreq.php


Example
-------
You'll find that the 12kb version already gets some of the point across, in some situations this might be good enough.
The highest quality sample has the most bytes and sounds best. Protracker will suggest to downsample it as it is
slightly over 22khz.

* input/ausline.wav 508kb


* output/ausline.wav/ausline-0-C-1.8svx 12kb
* output/ausline.wav/ausline-1-C#1.8svx 13kb
* output/ausline.wav/ausline-2-D-1.8svx 14kb
* output/ausline.wav/ausline-3-D#1.8svx 14kb
* output/ausline.wav/ausline-4-E-1.8svx 15kb
* output/ausline.wav/ausline-5-F-1.8svx 16kb
* output/ausline.wav/ausline-6-F#1.8svx 17kb
* output/ausline.wav/ausline-7-G-1.8svx 18kb
* output/ausline.wav/ausline-8-G#1.8svx 19kb
* output/ausline.wav/ausline-9-A-1.8svx 20kb
* output/ausline.wav/ausline-10-A#1.8svx 21kb
* output/ausline.wav/ausline-11-B-1.8svx 23kb
* output/ausline.wav/ausline-12-C-2.8svx 24kb
* output/ausline.wav/ausline-13-C#2.8svx 25kb
* output/ausline.wav/ausline-14-D-2.8svx 27kb
* output/ausline.wav/ausline-15-D#2.8svx 28kb
* output/ausline.wav/ausline-16-E-2.8svx 30kb
* output/ausline.wav/ausline-17-F-2.8svx 32kb
* output/ausline.wav/ausline-18-F#2.8svx 34kb
* output/ausline.wav/ausline-19-G-2.8svx 36kb
* output/ausline.wav/ausline-20-G#2.8svx 38kb
* output/ausline.wav/ausline-21-A-2.8svx 40kb
* output/ausline.wav/ausline-22-A#2.8svx 43kb
* output/ausline.wav/ausline-23-B-2.8svx 45kb
* output/ausline.wav/ausline-24-C-3.8svx 48kb
* output/ausline.wav/ausline-25-C#3.8svx 51kb
* output/ausline.wav/ausline-26-D-3.8svx 54kb
* output/ausline.wav/ausline-27-D#3.8svx 57kb
* output/ausline.wav/ausline-28-E-3.8svx 60kb
* output/ausline.wav/ausline-29-F-3.8svx 64kb
