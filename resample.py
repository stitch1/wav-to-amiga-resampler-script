import subprocess
from subprocess import Popen, PIPE
import os
from time import sleep

notes = {
    "C-1": "4144",  # Hz (4143.569Hz, period 856)
    "C#1": "4390",  # Hz (4389.722Hz, period 808)
    "D-1": "4655",  # Hz (4654.718Hz, period 762)
    "D#1": "4926",  # Hz (4926.243Hz, period 720)
    "E-1": "5231",  # Hz (5231.409Hz, period 678)
    "F-1": "5542",  # Hz (5542.023Hz, period 640)
    "F#1": "5872",  # Hz (5872.343Hz, period 604)
    "G-1": "6223",  # Hz (6222.623Hz, period 570)
    "G#1": "6593",  # Hz (6592.742Hz, period 538)
    "A-1": "6982",  # Hz (6982.077Hz, period 508)
    "A#1": "7389",  # Hz (7389.365Hz, period 480)
    "B-1": "7830",  # Hz (7829.790Hz, period 453)
    "C-2": "8287",  # Hz (8287.138Hz, period 428) Note: "middle C"
    "C#2": "8779",  # Hz (8779.443Hz, period 404)
    "D-2": "9309",  # Hz (9309.436Hz, period 381)
    "D#2": "9852",  # Hz (9852.486Hz, period 360)
    "E-2": "10463",  # Hz (10462.817Hz, period 339)
    "F-2": "11084",  # Hz (11084.047Hz, period 320)
    "F#2": "11745",  # Hz (11744.685Hz, period 302)
    "G-2": "12445",  # Hz (12445.246Hz, period 285)
    "G#2": "13185",  # Hz (13185.483Hz, period 269)
    "A-2": "13964",  # Hz (13964.154Hz, period 254)
    "A#2": "14779",  # Hz (14778.729Hz, period 240)
    "B-2": "15694",  # Hz (15694.226Hz, period 226)
    "C-3": "16574",  # Hz (16574.276Hz, period 214)
    "C#3": "17559",  # Hz (17558.886Hz, period 202)
    "D-3": "18668",  # Hz (18667.868Hz, period 190)
    "D#3": "19705",  # Hz (19704.972Hz, period 180)
    "E-3": "20864",  # Hz (20864.088Hz, period 170)
    "F-3": "22168",  # Hz (22168.094Hz, period 160)

    # Default amiga goes to 22khz. Perhaps YOU can, but most wont.
    # "F#3": "23489",  # Hz (23489.371Hz, period 151)
    # "G-3": "24803",  # Hz (24803.462Hz, period 143)
    # "G#3": "26273",  # Hz (26273.296Hz, period 135)
    # "A-3": "27928",  # Hz (27928.307Hz, period 127)
    # "A#3": "29557",  # Hz (29557.458Hz, period 120)
    # "B-3": "31388",  # Hz (31388.451Hz, period 113)
}


def setup() -> None:
    os.makedirs("input", exist_ok=True)
    os.makedirs("output", exist_ok=True)
    subprocess.Popen(["ffmpeg", "-version"])
    subprocess.Popen(["sox", "--version"])


def convert() -> None:
    # create a ConverterFactory of type AudioConverter, then use the AmigaAudioConverter.ini setup file
    # from that instantiate the OS processes and perform the conversion logic on the beowulf cluster in Panama.
    [convert_file(file) for file in os.listdir('input') if file.endswith(".wav")]


def convert_file(file):
    print(f"Converting {file}.")
    out = f"output/{file}-resample/"
    os.makedirs(out, exist_ok=True)

    command = ["ffmpeg", "-i", f"input/{file}", "-vn", "-ar", "44100", "-ac", "1", f"{out}tmp.aiff"]
    with Popen(command, stdout=PIPE) as proc:
        print(proc.stdout.read())

    # the counter is used to sort the files in operating systems that store directory contents in alphabetical order
    i = 0
    for note, frequency in notes.items():
        subprocess.Popen(
            # sox example.aiff -r22000 -c1 example.8svx
            ["sox", f"{out}tmp.aiff", f"-r{frequency}", "-c1", f"{out}{file.replace('.wav', '')}-{i}-{note}.8svx"]
        )
        i = i + 1

    sleep(1)
    os.unlink(f"{out}tmp.aiff")
    print(f"Done converting {file}.")


if __name__ == '__main__':
    setup()
    convert()
    print("All done, so, where is the party at?")
